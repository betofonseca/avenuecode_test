package com.avenuecode.test.service;

import com.avenuecode.test.model.Product;
import com.avenuecode.test.dto.ProductDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ProductServiceTest {

    @Autowired
    private ProductService productService;

    @Test
    public void getAllLite() {
        List<ProductDto> allWithoutRelationships = productService.findAllWithoutRelationships();
        Assert.assertTrue(allWithoutRelationships.size() > 0);
    }

    @Test
    public void testFind() {
        Product product = productService.find(0);
        Assert.assertNotNull(product);
    }

    @Test
    public void testFindLite() {
        ProductDto lite = productService.findLite(0);
        Assert.assertNotNull(lite);
    }

    @Test
    public void testSaveNewProduct() {
        Product product = new Product();
        product.setName("name");
        product.setDescription("description");

        Product savedProduct = productService.saveOrUpdate(product);

        Assert.assertNotNull(savedProduct.getId());
        Assert.assertEquals(product.getName(), savedProduct.getName());
        Assert.assertEquals(product.getDescription(), savedProduct.getDescription());
    }

    @Test
    public void testSaveNewProductWithChildren() {
        Product existingProduct = productService.find(0);

        Product product = new Product();
        product.setName("name");
        product.setDescription("description");
        product.setProducts(new ArrayList<>());
        product.getProducts().add(existingProduct);

        product = productService.saveOrUpdate(product);

        Product checkProduct = productService.find(0);

        Assert.assertEquals(product.getId(), checkProduct.getParentProduct().getId());
    }

    @Test
    public void testUpdateExistingProduct() {
        Product product = new Product();
        product.setName("name");
        product.setDescription("description");

        product = productService.saveOrUpdate(product);

        Product product1ToUpdate = new Product();
        product1ToUpdate.setId(product.getId());
        product1ToUpdate.setName(product.getName());
        product1ToUpdate.setDescription("diferent desc");

        Product savedProduct = productService.saveOrUpdate(product1ToUpdate);

        Assert.assertNotNull(savedProduct.getId());
        Assert.assertEquals(product1ToUpdate.getName(), savedProduct.getName());
        Assert.assertEquals(product1ToUpdate.getDescription(), savedProduct.getDescription());
    }

    @Test
    public void testDeleteExistingProduct() {
        Product product = new Product();
        product.setName("my custom name");
        product.setDescription("my custom description");

        Product savedProduct = productService.saveOrUpdate(product);

        productService.delete(product.getId());

        Product search = productService.find(savedProduct.getId());

        Assert.assertNull(search);
    }

}
