package com.avenuecode.test.service;

import com.avenuecode.test.model.Image;
import com.avenuecode.test.model.Product;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ImageServiceTest {

    @Autowired
    private ImageService imageService;

    @Test
    public void testSaveNewImage() {
        Image image = new Image();
        image.setType("jpg");
        image.setProductId(0);

        Image savedImage = imageService.saveOrUpdate(image.getProductId(), image);

        Assert.assertNotNull(savedImage.getId());
        Assert.assertEquals(image.getType(), savedImage.getType());
        Assert.assertEquals(image.getProduct().getId(), savedImage.getProduct().getId());
    }

    @Test
    public void testUpdateExistingImage() {
        Image image = new Image();
        image.setType("jpg");
        image.setProductId(0);

        image = imageService.saveOrUpdate(image.getProductId(), image);

        Image imageToUpdate = new Image();
        imageToUpdate.setId(image.getId());
        imageToUpdate.setType("png");
        imageToUpdate.setProductId(0);

        Image savedImage = imageService.saveOrUpdate(imageToUpdate.getProductId(), imageToUpdate);

        Assert.assertEquals(savedImage.getId(), imageToUpdate.getId());
        Assert.assertEquals(savedImage.getType(), imageToUpdate.getType());
        Assert.assertEquals(savedImage.getProduct().getId(), imageToUpdate.getProduct().getId());
    }

    @Test
    public void testDeleteExistingImage() {
        Image image = new Image();
        image.setType("jpg");
        image.setProductId(0);

        Image savedImage = imageService.saveOrUpdate(image.getProductId(), image);

        imageService.delete(savedImage.getProductId(), savedImage.getId());

        Image search = imageService.find(savedImage.getId());

        Assert.assertNull(search);
    }

}
