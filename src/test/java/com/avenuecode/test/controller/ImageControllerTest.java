package com.avenuecode.test.controller;

import com.avenuecode.test.model.Image;
import com.avenuecode.test.service.ImageService;
import com.avenuecode.test.util.IntegrationTestUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ImageControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private ImageService imageService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testCreateImageOk() throws Exception {
        mockMvc.perform(post("/products/0/images/")//
                .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)//
                .content("{\"type\":\"png\",\"productId\":\"0\"}"))//
                .andExpect(MockMvcResultMatchers.status().isCreated())//
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testCreateImageWithWrongProductIdRef() throws Exception {
        mockMvc.perform(post("/products/1/images/")//
                .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)//
                .content("{\"type\":\"png\",\"productId\":\"0\"}"))//
                .andExpect(MockMvcResultMatchers.status().isInternalServerError())//
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testUpdateImageOk() throws Exception {
        Image image = new Image();
        image.setProductId(0);
        image.setType("png");

        Image imageSaved = imageService.saveOrUpdate(image.getProductId(), image);

        mockMvc.perform(put("/products/0/images/")//
                .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)//
                .content("{\"id\":" + imageSaved.getId() + ",\"type\":\"png\",\"productId\":\"0\"}"))//
                .andExpect(MockMvcResultMatchers.status().isOk())//
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testUpdateImageWithWrongProductIdRef() throws Exception {
        Image image = new Image();
        image.setProductId(0);
        image.setType("png");

        Image imageSaved = imageService.saveOrUpdate(image.getProductId(), image);

        mockMvc.perform(put("/products/1/images/")//
                .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)//
                .content("{\"id\":" + imageSaved.getId() + ",\"type\":\"png\",\"productId\":\"0\"}"))//
                .andExpect(MockMvcResultMatchers.status().isInternalServerError())//
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testDeleteImageOk() throws Exception {
        Image image = new Image();
        image.setProductId(0);
        image.setType("png");

        Image imageSaved = imageService.saveOrUpdate(image.getProductId(), image);

        mockMvc.perform(delete("/products/0/images/" + imageSaved.getId())//
                .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8))//
                .andExpect(MockMvcResultMatchers.status().isOk())//
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testDeleteImageWithWrongProductIdRef() throws Exception {
        Image image = new Image();
        image.setProductId(0);
        image.setType("png");

        Image imageSaved = imageService.saveOrUpdate(image.getProductId(), image);

        mockMvc.perform(delete("/products/1/images/" + imageSaved.getId())//
                .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8))//
                .andExpect(MockMvcResultMatchers.status().isInternalServerError())//
                .andDo(MockMvcResultHandlers.print());
    }

}
