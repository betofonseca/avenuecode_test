package com.avenuecode.test.controller;

import com.avenuecode.test.model.Product;
import com.avenuecode.test.service.ProductService;
import com.avenuecode.test.util.IntegrationTestUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.hasToString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private ProductService productService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testFindAll() throws Exception {
        mockMvc.perform(get("/products/"))//
                .andExpect(MockMvcResultMatchers.status().isOk())//
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testImagesForProduct() throws Exception {
        mockMvc.perform(get("/products/0/images/set"))//
                .andExpect(MockMvcResultMatchers.status().isOk())//
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testProductsForProduct() throws Exception {
        mockMvc.perform(get("/products/0/products/set"))//
                .andExpect(MockMvcResultMatchers.status().isOk())//
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testFindAllLite() throws Exception {
        mockMvc.perform(get("/products/lite"))//
                .andExpect(MockMvcResultMatchers.status().isOk())//
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testFindBy() throws Exception {
        mockMvc.perform(get("/products/0"))//
                .andExpect(MockMvcResultMatchers.status().isOk())//
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", hasToString("Star Wars Complete Collection")))//
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testFindByLite() throws Exception {
        mockMvc.perform(get("/products/0/lite"))//
                .andExpect(MockMvcResultMatchers.status().isOk())//
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", hasToString("Star Wars Complete Collection")))//
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testCreateProductOk() throws Exception {
        Product product = new Product();
        product.setName("my custom name");
        product.setDescription("my custom description");

        mockMvc.perform(post("/products/")//
                .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)//
                .content(IntegrationTestUtil.convertObjectToJsonBytes(product)))//
                .andExpect(MockMvcResultMatchers.status().isCreated())//
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testUpdateProductOk() throws Exception {
        Product product = new Product();
        product.setName("my custom name");
        product.setDescription("my custom description");

        Product savedProduct = productService.saveOrUpdate(product);

        mockMvc.perform(put("/products/")//
                .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)//
                .content(IntegrationTestUtil.convertObjectToJsonBytes(savedProduct)))//
                .andExpect(MockMvcResultMatchers.status().isOk())//
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testDeleteProductOk() throws Exception {
        Product product = new Product();
        product.setName("my custom name");
        product.setDescription("my custom description");

        Product savedProduct = productService.saveOrUpdate(product);

        mockMvc.perform(delete("/products/" + product.getId())//
                .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8))//
                .andExpect(MockMvcResultMatchers.status().isOk())//
                .andDo(MockMvcResultHandlers.print());
    }

}
