package com.avenuecode.test.repository;

import com.avenuecode.test.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.stream.Stream;


public interface ProductRepository extends JpaRepository<Product, Integer> {

    @Query("select p from Product p where p.parentProduct.id = ?1")
    Stream<Product> findAllChildren(Integer parentId);
}
