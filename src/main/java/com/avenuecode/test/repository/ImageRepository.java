package com.avenuecode.test.repository;

import com.avenuecode.test.model.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.stream.Stream;


public interface ImageRepository extends JpaRepository<Image, Integer> {

    @Query("select i from Image i where i.product.id = ?1")
    Stream<Image> findAllByProduct(Integer productId);
}
