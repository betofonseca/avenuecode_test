package com.avenuecode.test.controller;

import com.avenuecode.test.model.Image;
import com.avenuecode.test.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/products/{productId}/images")
public class ImageController {

    @Autowired
    private ImageService imageService;

    @RequestMapping(method = RequestMethod.POST, value = "/")
    @ResponseStatus(HttpStatus.CREATED)
    public void save(@PathVariable Integer productId, @RequestBody Image image)
    {
        imageService.saveOrUpdate(productId, image);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/")
    @ResponseStatus(HttpStatus.OK)
    public void update(@PathVariable Integer productId, @RequestBody Image image)
    {
        imageService.saveOrUpdate(productId, image);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{imageId}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable Integer productId, @PathVariable Integer imageId)
    {
        imageService.delete(productId, imageId);
    }

}
