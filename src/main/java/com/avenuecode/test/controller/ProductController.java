package com.avenuecode.test.controller;


import com.avenuecode.test.model.Image;
import com.avenuecode.test.model.Product;
import com.avenuecode.test.dto.ProductDto;
import com.avenuecode.test.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping(method = RequestMethod.GET, value = "/")
    @ResponseStatus(HttpStatus.OK)
    public List<Product> findAll()
    {
        return productService.findAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{idProduct}")
    @ResponseStatus(HttpStatus.OK)
    public Product findBy(@PathVariable Integer idProduct)
    {
        return productService.find(idProduct);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{idProduct}/lite")
    @ResponseStatus(HttpStatus.OK)
    public ProductDto findByLite(@PathVariable Integer idProduct)
    {
        return productService.findLite(idProduct);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{idProduct}/images/set")
    @ResponseStatus(HttpStatus.OK)
    public List<Image> findImagesBy(@PathVariable Integer idProduct)
    {
        return productService.find(idProduct).getImages();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{idProduct}/products/set")
    @ResponseStatus(HttpStatus.OK)
    public List<Product> findProductsBy(@PathVariable Integer idProduct)
    {
        return productService.find(idProduct).getProducts();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/lite")
    @ResponseStatus(HttpStatus.OK)
    public List<ProductDto> findAllLite()
    {
        return productService.findAllWithoutRelationships();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/")
    @ResponseStatus(HttpStatus.CREATED)
    public void save(@RequestBody Product product)
    {
        productService.saveOrUpdate(product);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/")
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody Product product)
    {
        productService.saveOrUpdate(product);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{idProduct}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable Integer idProduct)
    {
        productService.delete(idProduct);
    }

}
