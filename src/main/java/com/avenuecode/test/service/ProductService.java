package com.avenuecode.test.service;

import com.avenuecode.test.model.Image;
import com.avenuecode.test.model.Product;
import com.avenuecode.test.dto.ProductDto;
import com.avenuecode.test.repository.ImageRepository;
import com.avenuecode.test.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ImageRepository imageRepository;

    @Transactional(readOnly = true)
    public List<Product> findAll() {
        List<Product> products = productRepository.findAll();
        products.forEach(product -> loadData(product));
        return products;
    }

    private void loadData(Product product) {
        product.getImages().forEach(each -> each.setProductId(each.getProduct().getId()));
        product.getProducts().forEach(each -> each.setParentProductId(each.getParentProduct().getId()));
    }

    @Transactional(readOnly = true)
    public Product find(Integer idProduct) {
        return productRepository.findOne(idProduct);
    }

    @Transactional(readOnly = true)
    public ProductDto findLite(Integer idProduct) {
        return productRepository.findOne(idProduct).toProductDto();
    }

    @Transactional(readOnly = true)
    public List<ProductDto> findAllWithoutRelationships() {
        List<ProductDto> productDtos = new ArrayList<>();
        Stream<Product> stream = productRepository.findAll().stream();
        stream.forEach(product -> productDtos.add(product.toProductDto()));
        return productDtos;
    }

    @Transactional
    public Product saveOrUpdate(Product product) {
        Product parentProduct = productRepository.saveAndFlush(product);

        if (product.getProducts() != null) {
            Stream<Product> children = product.getProducts().stream();
            children.forEach(child -> {
                child.setParentProduct(parentProduct);
                productRepository.saveAndFlush(child);
            });
        }

        return parentProduct;
    }

    @Transactional
    public void delete(Integer idProduct) {
        Stream<Product> allChildrenProducts = productRepository.findAllChildren(idProduct);
        allChildrenProducts.forEach(child -> {
            Stream<Image> images = imageRepository.findAllByProduct(child.getId());
            images.forEach(image -> imageRepository.delete(image));

            productRepository.delete(child);
        });

        Stream<Image> images = imageRepository.findAllByProduct(idProduct);
        images.forEach(image -> imageRepository.delete(image));
        productRepository.delete(idProduct);
    }

}
