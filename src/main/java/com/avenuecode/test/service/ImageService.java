package com.avenuecode.test.service;


import com.avenuecode.test.exception.InternalServerErrorException;
import com.avenuecode.test.model.Image;
import com.avenuecode.test.model.Product;
import com.avenuecode.test.repository.ImageRepository;
import com.avenuecode.test.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ImageService {

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private ProductRepository productRepository;

    @Transactional
    public Image saveOrUpdate(Integer productId, Image image) {
        if (image.getProductId() != null && productId != image.getProductId()) {
            throw new InternalServerErrorException();
        }
        Product product = productRepository.findOne(image.getProductId());
        image.setProduct(product);
        return imageRepository.saveAndFlush(image);
    }

    @Transactional
    public void delete(Integer productId, Integer imageId) {
        Image image = find(imageId);
        if (productId != image.getProduct().getId()) {
            throw new InternalServerErrorException();
        }
        imageRepository.delete(imageId);
    }

    @Transactional(readOnly = true)
    public Image find(Integer imageId) {
        return imageRepository.findOne(imageId);
    }

}
