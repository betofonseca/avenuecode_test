insert into "product" ("id", "parent_product_id", "name", "description") VALUES (0, null, 'Star Wars Complete Collection', 'The full Star Wars Collection')
insert into "product" ("id", "parent_product_id", "name", "description") VALUES (1, 0, 'Rogue One: A Star Wars Story', 'Rogue One film movie')
insert into "product" ("id", "parent_product_id", "name", "description") VALUES (2, 0, 'Star Wars: Death Star', 'Star Wars: Death Star movie')

insert into "image" ("id", "product_id", "type") VALUES (3, 0, 'image of collection')
insert into "image" ("id", "product_id", "type") VALUES (4, 1, 'image of rogue one')
insert into "image" ("id", "product_id", "type") VALUES (5, 2, 'image of death star')
insert into "image" ("id", "product_id", "type") VALUES (6, 2, 'image of darth vader')
