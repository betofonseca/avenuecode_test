# Solution

The solution was entirely developed using Spring Boot. I had some problems to deal with infinite recursion because of
jpa tries to load a product that has a list of images that has a list of products.. But at the end I decided to create
a transient variable inside Image and Product model to receive the reference id and ignored the real data from
relationship, so I had to update the way that I receive information (create/update) to get this info from the transient
var.


### Run tests

To run all tests, just execute a maven command on root project directory:

```sh
$ mvn test
```

### Build application

To compile application and create a executable jar:

```sh
$ mvn package
```

### Execute application

Application developed using Spring Boot, so to run it just execute a maven command on root project directory:

```sh
$ mvn spring-boot:run
```

Another option to run this app is to build the app first, than execute the generated JAR directly (on root project directory):

```sh
$ java -jar target/jouberto-0.0.1-SNAPSHOT.jar
```

### Swagger

To simplify the way to interact with the app, it's possible to check all end point's info by looking at http://localhost:8080/v2/api-docs (JSON).


### Unit test

All functional requirements is created and tested.
